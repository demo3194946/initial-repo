function_name="abctest"
runtime="python3.10"
subnet_ids=["subnet-0fd0389467e854d58","subnet-0c5a552da99449eee", "subnet-0a88054dd4eb10909"]
security_group_ids=["sg-0cc4c5c91bcdc307b"]
description="test backstage"
api_gate_way_name="abctest"
statefile_name="abctest"

dynamodb_partition_key="abctest"

dynamodb_table_name="abctest"